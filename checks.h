#ifndef CHECKSH
#define CHECKSH

#include <stdio.h>
#include <string.h>
#include "LinkedList.h"

FrameNode* nameFound(FrameNode* list, char* name);
int exists(char *fname);
int getListLength(FrameNode* head);

#endif