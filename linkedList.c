#include <stdlib.h>
#include <string.h>
#include "linkedList.h"
#include "checks.h"

/*
function to create a frame node (a node of the frame node's list)
input: frame's path string, frame's duration, frame's name
output: the frame node
*/
FrameNode* createFrameNode(char* framePath, int duration, char* name)
{
	Frame* frame = (Frame*)malloc(sizeof(Frame));
	FrameNode* frameNode = (FrameNode*)malloc(sizeof(FrameNode));

	frameNode->frame = frame;

	frameNode->frame->path = (char*)malloc(sizeof(char) * (strlen(framePath) + 1)); //dynamic allocation
	memset(frameNode->frame->path, 0, strlen(framePath) + 1);
	strcpy(frameNode->frame->path, framePath); //path in frame struct is declared by pointer only without size and therfore we must dynamically allocate it first

	frame->duration = duration;

	frameNode->frame->name = (char*)malloc(sizeof(char) * (strlen(name) + 1));
	memset(frameNode->frame->name, 0, strlen(name) + 1);
	strcpy(frameNode->frame->name, name); //same

	frameNode->next = NULL;

	return frameNode;
}

/*
function to add a frame node to the end of the frame nodes list
input: frame node list head, new frame node
output: none
*/
void addFrame(FrameNode** head, FrameNode* newFrame)
{
	FrameNode* curr = *head;
	if (!*head) // empty list!
	{
		*head = newFrame;
	}
	else
	{
		while (curr->next) // while the next is NOT NULL (when next is NULL - that is the last node)
		{
			curr = curr->next;
		}

		curr->next = newFrame;
		newFrame->next = NULL;
	}
}

/*
function to delete a frame node by frame's name
input: frame node list head, name string
output: none
*/
void deleteNode(FrameNode** head, char* name)
{
	FrameNode* curr = *head;
	FrameNode* temp = NULL;

	// if the list is not empty (if list is empty - nothing to delete!)
	if (*head)
	{
		// the first node should be deleted?
		if (0 == strcmp((*head)->frame->name, name))
		{
			*head = (*head)->next;
			free(curr->frame->path); //free dynamically allocated variables
			free(curr->frame->name);
			free(curr->frame); //free frame
			free(curr); //delete the node
		}
		else
		{
			while (curr->next)
			{
				if ((0 == strcmp(curr->next->frame->name, name))) // waiting to be on the node BEFORE the one we want to delete
				{
					temp = curr->next; // put aside the node to delete
					curr->next = temp->next; // link the node before it, to the node after it
					free(temp->frame->path); //free dynamically allocated variables
					free(temp->frame->name);
					free(temp->frame); //free frame
					free(temp); // delete the node
				}
				else
				{
					curr = curr->next;
				}
			}
		}
	}
}

/*
function to replace a frame node given it's frame name to anywhere else on the list given the index(change a frame's index function)
input: frame node list head, frame's name string, new index
output: none
*/
void changeFrameIndex(FrameNode** head, char* name, int index)
{
	int i = 1;
	FrameNode* changeFrame = nameFound(*head, name);
	FrameNode* curr = *head;
	FrameNode* temp = NULL;

	if (*head) //if list is empty there is nothing to change
	{
		if (changeFrame == *head) //if wanted frame is the first
		{
			*head = (*head)->next;
		}
		else
		{
			while (curr && curr->next)
			{
				if (curr->next == changeFrame)
				{
					curr->next = curr->next->next;
				}
				curr = curr->next;
			}
		}
		//inserting wanted frame to wanted index
		if (index == 1) //if frame should be changed to first index
		{
			temp = *head;
			*head = changeFrame;
			(*head)->next = temp;
		}
		else
		{
			curr = *head;
			while (i < index - 1)
			{
				curr = curr->next;
				i++;
			}
			changeFrame->next = curr->next;
			curr->next = changeFrame;
		}
	}
}

/*
function to change a duration of all frames in frame nodes list
input: frame node list head, duration
output: none
*/
void changeDurations(FrameNode* head, int duration)
{
	FrameNode* curr = head;
	while (curr)
	{
		curr->frame->duration = duration;
		curr = curr->next;
	}
}

/*
function to free a frame nodes list
input: frame node list head
output: none
*/
void freeList(FrameNode** head)
{
	FrameNode* curr = *head;
	FrameNode* temp = NULL;
	while (curr)
	{
		temp = curr;
		curr = curr->next;
		free(temp->frame->path);
		free(temp->frame->name);
		free(temp->frame);
		free(temp);
	}
	*head = NULL;
}