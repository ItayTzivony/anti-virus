#ifndef LINKEDLISTH
#define LINKEDLISTH

#define FALSE 0
#define TRUE !FALSE

// Frame struct
typedef struct Frame
{
	char*		name;
	unsigned int	duration;
	char*		path;
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;

FrameNode* createFrameNode(char* framePath, int duration, char* name);
void addFrame(FrameNode** head, FrameNode* newFrame);
void deleteNode(FrameNode** head, char* name);
void changeFrameIndex(FrameNode** head, char* name, int index);
void changeDurations(FrameNode* head, int duration);

#endif
