#include <stdio.h>
#include <string.h>
#include "view.h"
#include "linkedList.h"
#include "checks.h"
#define STR_LEN 50

void printMenu(void);

int main(void)
{
	const char NEW_LINE[] = "\n";
	int choice = -1, duration = 0, index = 0, listLength = 0;
	char framePath[STR_LEN] = { 0 };
	char name[STR_LEN] = { 0 };
	FrameNode* head = NULL;

	while (choice != 0)
	{
		printMenu();
		scanf("%d", &choice);
		getchar();
		switch (choice)
		{
		case 0:
			printf("Goodbye!");
			break;
		case 1:
			printf("*** Creating new frame ***\nPlease insert frame path:\n"); //getting input
			fgets(framePath, STR_LEN, stdin);
			strtok(framePath, NEW_LINE);
			printf("Please insert frame duration(in miliseconds):\n");
			scanf("%d", &duration);
			getchar();
			printf("Please choose a name for that frame:\n");
			fgets(name, STR_LEN, stdin);
			strtok(name, NEW_LINE);

			if (!exists(framePath)) //check if exists
			{
				printf("Can't find file! Frame will not be added\n");
			}
			else
			{
				while (nameFound(head, name)) //verifying untaken name
				{
					printf("The name is already taken, please enter another name:\n");
					fgets(name, STR_LEN, stdin);
					strtok(name, NEW_LINE);
				}
				addFrame(&head, createFrameNode(framePath, duration, name)); //calling the function
			}

			break;
		case 2:
			printf("Enter the name of the frame you wish to erase:\n"); //input
			fgets(name, STR_LEN, stdin);
			strtok(name, NEW_LINE);
			if (!nameFound(head, name)) //verify
			{
				printf("The frame was not found\n");
			}
			else
			{
				deleteNode(&head, name); //call function
			}

			break;
		case 3:
			printf("Enter the name of the frame:\n"); //input
			fgets(name, STR_LEN, stdin);
			strtok(name, NEW_LINE);
			if (!nameFound(head, name)) //verify
			{
				printf("this frame does not exist\n");
			}
			else
			{
				printf("Enter the new index in the movie you wish to place the frame:\n"); //input
				scanf("%d", &index);
				getchar();
				listLength = getListLength(head);
				while (index < 1 || index > listLength) //verify
				{
					printf("The movie contains only %d frames!\nEnter the new index in the movie you wish to place the frame:\n", listLength);
					scanf("%d", &index);
					getchar();
				}
				changeFrameIndex(&head, name, index); //call function
			}

			break;
		case 4:
			printf("enter the name of the frame:\n"); //input
			fgets(name, STR_LEN, stdin);
			strtok(name, NEW_LINE);
			if (!(nameFound(head, name))) //verify
			{
				printf("The frame does not exist\n");
			}
			else
			{
				printf("Enter the new duration:\n"); //input
				scanf("%d", &duration);
				getchar();
				nameFound(head, name)->frame->duration = duration; //changing duration
			}

			break;
		case 5:
			printf("Enter the duration for all frames:\n"); //input
			scanf("%d", &duration);
			getchar();
			changeDurations(head, duration); //call function

			break;
		case 6:
			printFramesList(head); //calling printing function

			break;
		case 7:
			play(head); //calling video playing function
			break;
		case 8:
			printf("TODO 8"); //part B

			break;
		default:
			printf("Only numbers between (0-8), try again."); //verifying input
			break;
		}
	}

	freeList(&head); //freeing alloceted list
	getchar();
	return 0;
}

/*
function to print main menu
input: none
output: none
*/
void printMenu(void)
{
	printf("\nWhat would you like to do?\n");
	printf(" [0] Exit\n");
	printf(" [1] Add new frame\n");
	printf(" [2] Remove a frame\n");
	printf(" [3] Change frame index\n");
	printf(" [4] Change frame duration\n");
	printf(" [5] Change duration of all frames\n");
	printf(" [6] List frames\n");
	printf(" [7] Play movie!\n");
	printf(" [8] Save project\n");
}