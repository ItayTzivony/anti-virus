#include "checks.h"

/*
function to check if a given name of frame is found in any of the frames in the frames list, if found will return frame node pointer
input: head of frames list, name string
output: frame node pointer if found by name or NULL otherwise
*/
FrameNode* nameFound(FrameNode* head, char* name)
{
	int found = FALSE;
	FrameNode* curr = head;
	while (curr && !found)
	{
		if (strcmp(curr->frame->name, name) == 0)
		{
			found = TRUE;
		}
		else
		{
			curr = curr->next;
		}
	}
	return curr;
}

/*
function to check if a file exissts in the given file path
input: file name(path) string
output: TRUE/FALSE value
*/
int exists(char *fname)
{
	int exists = FALSE;
	FILE *file;
	if ((file = fopen(fname, "r")))
	{
		fclose(file);
		exists = TRUE;
	}
	return exists;
}

/*
function to get frames node list's length
input: frame node list head
output: length
*/
int getListLength(FrameNode* head)
{
	int i = 0;
	FrameNode* curr = head;
	while (curr)
	{
		i++;
		curr = curr->next;
	}
	return i;
}